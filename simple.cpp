#include <iostream>
#include "Eigen/Dense"
#include "Eigen/Sparse"

typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double

int main() {
    int nr = 30000;
    int nc = 3000;
    SpMat X(nr, nc);
    X.makeCompressed();

    for (int i = 5; i < 50; i += 5) {
        Eigen::MatrixXd B(i, i);
        B.setZero(i, i);
        Eigen::BDCSVD<Eigen::MatrixXd> svd(B, Eigen::ComputeThinU | Eigen::ComputeThinV);
        std::cout << ">> " << i << "\t" << svd.singularValues()[0] << std::endl;
    }

    return 0;
}
